# Neural networks

## Adam Mickiewicz University - Faculty of Mathematics and Computer Science

## Exercise solutions

* [McCulloch-Pitts neuron model](Python/ex1.py)
* [Perceptron](Python/ex2.py)
* [Hebbian learning](Python/ex3.py)
* [Gradient descent](Python/ex4.py)
* [Backpropagation](Python/ex5.py)
* [Autoencoder](Python/ex6.py) - unfinished
* [Hopfield network](Python/ex7.py)
* [Boltzmann Machine](Python/ex8.py)
* [Travelling salesman problem](Python/ex9.py)
* [Factorization](Python/ex10.py)