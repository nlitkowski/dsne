# Litkowski Norbert
# Cw.10
# Python

# Factorization

import numpy
import random

def main():
    numbers = [12, 91, 143, 1737, 1859, 988027]
    for num in numbers:
        try:
            factorize(num)
        except ZeroDivisionError as e:
            print(f"Error: {str(e)}")
        except RecursionError as e:
            print(f"Error: {str(e)}")

def gcd(a, b): 
    if a % b == 0:
        return b
    else:
        return gcd(b, a % b)
 
def solve_DL(a, n):
    r = 1
    while power(a, r) % n != 1 and r < 10:
        r += 1
    if r == 10:
        return -1
    else:
        return r

def power(a, b):
    result = 1
    for i in numpy.arange(b):
        result = result * a
    return result 
 
def factorize(n):
    random_number = round(random.random() * 100000000) % n
 
    if (gcd(n, random_number) > 1):
        print(f"Number: {n}, Random Number: {random_number}, GCD: {gcd(n, random_number)}")
    else:
        r = solve_DL(random_number, n)
        # print(f"R: {r}")
        if r == -1:
            factorize(n)
        elif r % 2 == 0:
            if gcd(n, power(random_number, r / 2) + 1) > 1:
                print(f"Number: {n}, Random Number: {random_number}, Power: {power(random_number, r / 2) + 1}")
            elif gcd(n, power(random_number, r / 2) - 1) > 1:
                print(f"Number: {n}, Random Number: {random_number}, Power: {power(random_number, r / 2) - 1}")
            else:
                factorize(n)
        else:
            factorize(n)
 
if __name__ == "__main__":
    main()
