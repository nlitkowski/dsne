# Litkowski Norbert
# Cw.9
# Python

# TSP
 
import math
import random
 
def main():
    # city_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    L = 3
    M = 150
    T0 = 1.0 

    simmulated_annealing(T0, L, M)

    simmulated_annealing_2(T0, L, M)
 
def get_dist(a, b):
    if (a == 1 and b == 10) or (a == 10 and b == 1):
        d = 1
    else:
        d = abs(a - b)
    return d
 
def change_t(T, T0, t):
    T = T0 / (1 + math.log(t + 1))
    return T
  
def change_s(S):
    r = random.sample(range(0, 10), 2)
    ch = S[r[0]]
    S[r[0]] = S[r[1]]
    S[r[1]] = ch
    return S
 
def cost(S):
    e = 0
    for i in range(0, 9):
        e = e + get_dist(S[i], S[i + 1])
    e = e + get_dist(S[9], S[0])
    return e
 
def simmulated_annealing(T0, L, M):
    S = random.sample(range(1, 11), 10)
    E_old = cost(S)
    T = T0
    ar = 0
    while (ar <= L):
        t = 0
        ar = 0
        while (t < M):
            S_old = S[:]
            E_old = cost(S_old)
            S_new = change_s(S_old)
            E_new = cost(S_new)
            deltaE = E_old - E_new
            if ((deltaE < 0) | (random.uniform(0, 1) < math.exp(-deltaE / T))):
                S = S_new
                ar = ar + 1
            t = t + 1
        T = change_t(T, T0, t)
    print("\nSA 1")
    print("S: ", S)
    print("Cost: ", cost(S))
 
def get_dist_2(a, b):
    if (a == 1 and b == 2) or (a == 9 and b == 10):
        d = 1
    elif (a < b):
        # a^3 + b^3 - a^2b - ab^2 + 4a^2 -4b^2 + 4a - 4b + 1
        d = a ** 3 + b ** 3 - (a ** 2) * b - a * (b ** 2) + 4 * (a ** 2) - 4 * (b ** 2)  + 4 * a - 4 * b + 1
    elif (a > b):
        d = get_dist(a, b)
    elif (a == b):
        d = 0
    return d
 
def cost2(S):
    e = 0
    for i in range(0, 9):
        e = e + get_dist_2(S[i], S[i + 1])
    e = e + get_dist_2(S[9], S[0])
    return e
 
def simmulated_annealing_2(T0, L, M):
    S = random.sample(range(1, 11), 10)
    E_old = cost2(S)
    T = T0
    ar = 0
    while (ar <= L):
        t = 0
        ar = 0
        while (t < M):
            S_old = S[:]
            E_old = cost2(S_old)
            S_new = change_s(S_old)
            E_new = cost2(S_new)
            deltaE = E_old - E_new
            if deltaE < 0 or (random.uniform(0, 1) < math.exp(-deltaE / T)):
                S = S_new
                ar = ar + 1
            t = t + 1
        T = change_t(T, T0, t)
    print("\nSA 2")
    print("S: ", S)
    print("Cost: ", cost2(S))

if __name__ == "__main__":
    main()